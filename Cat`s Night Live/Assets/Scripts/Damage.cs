﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// Klasa pozwalia nanosić uszkodzenie graczu lub przeciwiku 
/// </summary>
public class Damage : MonoBehaviour {

    public float damage;
    public string tag;

    public Text textGhosts;
    private int ghost;

    public float deltaTime;
    private float timeNext = 0;

    private void Start()
    {
        SaveStats stats = new SaveStats();
        ghost = stats.Ghosts;

        if(textGhosts != null)
            textGhosts.text = ghost.ToString();
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag.Equals(tag))
        {
            Health health = collision.gameObject.GetComponent<Health>();
            if (Time.time > timeNext)
            {
                ++ghost;
                if(textGhosts != null)
                    textGhosts.text = ghost.ToString(); 

                timeNext = Time.time + deltaTime;
                health.addDamage(damage);
            }
        }
    }

    /// <summary>
    /// getter dla otrzymania ilości zabitych wrogów
    /// </summary>
    public int Ghost{
        get{ return ghost; }
    }
}
