﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Generator : MonoBehaviour {


    
    public Transform point1;
    public Transform point2;

    public GameObject ghost1;
    public float dTimeGhost1;
    float nextGhost1;

    public GameObject ghost2;
    public float dTimeGhost2;
    float nextGhost2;

    public GameObject ghost3;
    public float dTimeGhost3;
    float nextGhost3;

    public GameObject ghost4;
    public float dTimeGhost4;
    float nextGhost4;

    public GameObject ghost5;
    public float dTimeGhost5;
    float nextGhost5;

    float timeHard;
    public float timeHardDelta;

    public Text textTime;
    float timeNext;
    int timeText = 0;

    private void Start()
    {
        SaveStats stats = new SaveStats();
        timeText = stats.Time;
        textTime.text = TimeText.ToString();

        timeHard = Time.time + 45f;


        nextGhost1 = dTimeGhost1;
        nextGhost2 = dTimeGhost2;
        nextGhost3 = dTimeGhost3;
        nextGhost4 = dTimeGhost4;
        nextGhost5 = dTimeGhost5;
    }

    // Update is called once per frame
    void Update () {
        float time = Time.time;
        

        if (timeNext < time)
        {
            timeNext = time + 1f;
            ++timeText;
            textTime.text = timeText.ToString();
        }

        if (nextGhost1 < time)
        {
            nextGhost1 = time + dTimeGhost1;
            generate(ghost1);
        }

        if (nextGhost2 < time)
        {
            nextGhost2 = time + dTimeGhost2;
            generate(ghost2);
        }

        if (nextGhost3 < time)
        {
            nextGhost3 = time + dTimeGhost3;
            generate(ghost3);
        }

        if (nextGhost4 < time)
        {
            nextGhost4 = time + dTimeGhost4;
            generate(ghost4);
        }

        if (nextGhost5 < time)
        {
            nextGhost5 = time + dTimeGhost5;
            generate(ghost5);
        }

        if(timeHard < time)
        {
            timeHard = time + timeHardDelta;
            if (dTimeGhost1 >= 1) dTimeGhost1 -= 0.1f;
            if (dTimeGhost2 >= 2) dTimeGhost2 -= 0.2f;
            if (dTimeGhost3 >= 3) dTimeGhost3 -= 1f;
            if (dTimeGhost4 >= 5) dTimeGhost4 -= 1.5f;
            if (dTimeGhost5 >= 10) dTimeGhost5 -= 2;
        }
    }

    void generate(GameObject ghost)
    {
        if (Random.RandomRange(0, 2) == 1)
        {
            Instantiate(ghost, point1.position, Quaternion.Euler(new Vector3(0, 0, 0)));
        }
        else
            Instantiate(ghost, point2.position, Quaternion.Euler(new Vector3(0, 0, 0)));
    }

   public int TimeText
    {
        get { return timeText; }
    }
}
