﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Zadaniem tej klasy jest przesunięcie gracza do pozycji kliknięcia myszy
/// </summary>
public class CatControler : MonoBehaviour {

    public float speed;
    private Rigidbody2D body;
    private Vector2 mousePos = Vector2.zero;
    private bool facingRight;
    private bool grounded;
    private bool move;

    private Animator animator;


    /// <summary>
    /// zmienne dla sparwdzania czy kot stoje
    /// </summary>
    public LayerMask groundLayer;
    public Transform groundCheck;
    private float groundCheckValue = 0.1f;


    public GameObject playerMenu;

    // Use this for initialization
    void Start () {
        body = GetComponent<Rigidbody2D>();
        facingRight = true;

        animator = GetComponent<Animator>();

    }
	
	void Update () {
        
        if (Input.GetMouseButtonDown(0) && grounded)
        {
            mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y));
            body.gravityScale = 0;
            move = true;
            grounded = false;
        }
        if (body.position == mousePos)
        {
            Fall();
            body.gravityScale = 1;
            move = false;
        }

        if (move)
        {
            transform.position = Vector3.MoveTowards(transform.position, mousePos, speed * Time.deltaTime);
            Jump();
        }
        if (grounded)
            Stay();

        if (Input.GetKeyDown("escape"))
        {

            if (Time.timeScale == 1.0)
            {
                playerMenu.SetActive(true);
                Time.timeScale = 0.0001f;
            }
            else
            {
                Time.timeScale = 1.0f;
                playerMenu.SetActive(false);
            }
        }
    }

    private void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckValue, groundLayer);

        if (body.velocity.x < mousePos.x && !facingRight)
            flip();
        else if (body.velocity.x > mousePos.x && facingRight)
            flip();


    }
    /// <summary>
    /// metoda włąncza animacje skoku 
    /// </summary>
    void Jump()
    {
        animator.SetBool("isStay", false);
        animator.SetBool("isFall", false);
        animator.SetBool("isJump", true);
    }

    /// <summary>
    /// funkcja która włącza animacje upadku
    /// </summary>
    void Fall()
    {
        animator.SetBool("isStay", false);
        animator.SetBool("isFall", true);
        animator.SetBool("isJump", false);
    }

    /// <summary>
    /// funkca dla animacji zostania
    /// </summary>
    void Stay()
    {
        animator.SetBool("isStay", true);
        animator.SetBool("isFall", false);
        animator.SetBool("isJump", false);
    }

    void Sit()
    {
        animator.SetBool("isStay", false);
        animator.SetBool("isFall", false);
        animator.SetBool("isJump", false);
    }


    /// <summary>
    /// metoda pozwalia obrut gracza w tą strone gdzie było zrobione kliknięcie
    /// </summary>
    void flip() {
        facingRight = !facingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
 
};
