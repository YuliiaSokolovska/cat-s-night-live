﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Zadaniem klasy jest przesunięcie przeciwnika
/// </summary>
public class EnemyControler : MonoBehaviour {

    public Vector3 target;
    public float speed;
    bool facingRight;


    private void Start()
    {
        facingRight = true;
    }
    // Update is called once per frame
    void Update() {

        if (transform.position.x < target.x && !facingRight)
            flip();
        else if (transform.position.x > target.x && facingRight)
            flip();

        if (target != Vector3.zero)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
            Debug.Log("Target " + target);
        }
    }

    /// <summary>
    /// metoda pozwalia obrut przeciwnika w tą strone gdzie on idzie 
    /// </summary>
    void flip()
    {
        facingRight = !facingRight;
        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;
    }
}
