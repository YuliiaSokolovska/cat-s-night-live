﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Klasa do obslugi menu w grze
/// </summary>
public class MenuListener : MonoBehaviour {

    public GameObject cat;
    public GameObject girl;
    public GameObject ghosts;

    private void Start()
    {
        Time.timeScale = 1.0f;
    }

    /// <summary>
    /// funkcja która wlącza nową gre
    /// </summary>
    public void NewGame()
    {

        SaveStats stats = new SaveStats();
        stats.DeleteAll();
        SceneManager.LoadScene("Scene2", LoadSceneMode.Single);
    }
    /// <summary>
    /// metoda do przedłużenia grzy
    /// </summary>
    public void Resume()
    {
        SceneManager.LoadScene("Scene2", LoadSceneMode.Single);
    }

    public void Restart()
    {
        SaveStats stats = new SaveStats();
        stats.DeleteAll();
        SceneManager.LoadScene("Scene2", LoadSceneMode.Single);
    }
    
    /// <summary>
    /// wrócić się do glównego menu
    /// </summary>
    public void BackToMainMenu()
    {
        if (cat != null && girl != null && ghosts != null)
        {
            Health health = girl.GetComponent<Health>();
            Damage ghosts = cat.GetComponent<Damage>();
            Generator time = ghosts.GetComponent<Generator>();

            SaveStats stats = new SaveStats();
            stats.Save(health, ghosts, time);
        }
        SceneManager.LoadScene("Scene1", LoadSceneMode.Single);
    } 

    /// <summary>
    /// zamknąć aplikacje
    /// </summary>
    public void Exit()
    {
        Time.timeScale = 1.0f;
        Application.Quit();
    }
}
