﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///Zadaniem tej klasy jest zapisanie oraz odczyt postępu w gre 
/// </summary>
public class SaveStats : MonoBehaviour {

    private const string HEALTH = "health";
    private const string TIME = "timess";
    private const string GHOSTS = "ghosts";

    /// <summary>
    /// metoda zapisuje postęp
    /// </summary>
    /// <param name="health">obiekt który zarządza zdrowiem</param>
    /// <param name="ghosts">obiekt potrzebny do otrzymania ilości zabitych wrgów</param>
    /// <param name="generator">w tym obiekcie zachowuje się czas gre</param>
    public void Save(Health health, Damage ghosts, Generator generator)
    {
        if (health != null) PlayerPrefs.SetFloat(HEALTH, health.CurrentHealth);
        if (generator != null) PlayerPrefs.SetInt(TIME, generator.TimeText);
        if (ghosts != null) PlayerPrefs.SetInt(GHOSTS, ghosts.Ghost);
        PlayerPrefs.Save();

    }
    /// <summary>
    /// metoda do zapisania życia
    /// </summary>
    /// <param name="value"></param>
    public void SaveHeath(float value)
    {
        PlayerPrefs.SetFloat(HEALTH, value);
        PlayerPrefs.Save();
    }

    /// <summary>
    /// metoda do zapisania czasu
    /// </summary>
    /// <param name="value"></param>
    public void SaveTime(int value)
    {
        PlayerPrefs.SetInt(TIME, value);
        PlayerPrefs.Save();
    }

    /// <summary>
    /// metoda do zapisania ilości zabitych wrogów
    /// </summary>
    /// <param name="value"></param>
    public void SaveGhosts(int value)
    {
        PlayerPrefs.SetInt(GHOSTS, value);
        PlayerPrefs.Save();
    }

    /// <summary>
    /// funkcja usunięcia wszystkie zapisaniej informacji
    /// </summary>
    public void DeleteAll()
    {
        PlayerPrefs.DeleteAll();
    }

    public int Time
    {
        get { return PlayerPrefs.GetInt(TIME, 0); }
    }

    public int Ghosts
    {
        get { return PlayerPrefs.GetInt(GHOSTS, 0); }
    }

    public float Health
    {
        get { return PlayerPrefs.GetFloat(HEALTH, 100); }
    }

}
