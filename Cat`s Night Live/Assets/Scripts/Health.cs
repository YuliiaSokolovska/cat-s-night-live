﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Klasa która zarządza zdrowiem przeciwnika lub kobiety
/// </summary>
public class Health : MonoBehaviour {

    public float Maxhealth;
    public Slider slider;
    private float health;
    public bool isGirl;

    private void Start()
    {
        if (isGirl)
        {
            SaveStats stats = new SaveStats();
            health = stats.Health;
        }

        if(health == 0)
            health = Maxhealth;
        if (slider != null)
        {
            slider.maxValue = Maxhealth;
            slider.value = health;
        }
    }

    /// <summary>
    /// metoda naniesienia uszkodzeń
    /// </summary>
    /// <param name="damage">
    /// uszkodzenie</param>
    public void addDamage(float damage)
    {
        health -= damage;
        if(slider != null)
            slider.value = health;
        Debug.Log(health.ToString());
        if (health <= 0)
            if (isGirl)
                SceneManager.LoadScene("Scene3", LoadSceneMode.Single);
            else Destroy(gameObject);


    }


    public float CurrentHealth
    {
        get { return health; }
    }
}
